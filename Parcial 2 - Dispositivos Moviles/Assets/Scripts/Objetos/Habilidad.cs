using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habilidad : MonoBehaviour
{
    private Rigidbody2D rb;
    public static bool estaActiva = false; // Indica si la habilidad est� activa
    private pausaYderrota hud;
    private Movimiento movimiento;
    private ControlVelocidad controlvelocidad;
    private float desplazamiento = 2.0f;

    void Awake()
    {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        hud = FindObjectOfType<pausaYderrota>();
        movimiento = FindObjectOfType<Movimiento>();
        controlvelocidad = FindObjectOfType<ControlVelocidad>();
    }

    void OnEnable()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void Update()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (hud != null)
            {
                hud.ActualizarHabilidad(true, 1);
            }
            movimiento.audioPowerUP.Play();
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Pinchos")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}









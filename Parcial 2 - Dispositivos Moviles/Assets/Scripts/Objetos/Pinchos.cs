using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinchos : MonoBehaviour
{
    private Rigidbody2D rb;
    private pausaYderrota hud;
    private Movimiento movimiento;
    private ControlVelocidad controlvelocidad;

    public void Awake()
    {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        hud = FindObjectOfType<pausaYderrota>();
        movimiento = FindObjectOfType<Movimiento>();
        controlvelocidad = FindObjectOfType<ControlVelocidad>();
    }

    void OnEnable()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void Update()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            movimiento.audioPinchos.Play();
            hud.Muerte();
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
            Destroy(gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Monedas : MonoBehaviour
{
    private Rigidbody2D rb;
    public static float ContadorMonedas;
    public static float RecordMonedas; 
    private pausaYderrota hud;
    private Movimiento movimiento;
    //Pooling
    private Action<Monedas> desactivarAccion;
    private bool EstaDesactivado = false;

    private GameObject jugador;
    private ControlVelocidad controlvelocidad;
    private float desplazamiento = 2.0f;

    void Awake()
    {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        jugador = GameObject.FindGameObjectWithTag("Player");
        hud = FindObjectOfType<pausaYderrota>();
        movimiento = FindObjectOfType<Movimiento>();
        controlvelocidad = FindObjectOfType<ControlVelocidad>();
        LoadMonedas(); // Cargar el contador de monedas y el r�cord al inicio
        ContadorMonedas = 0;
    }

    void Start()
    {

    }

    void OnEnable()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }

        EstaDesactivado = false;
    }

    void Update()
    {
        if (jugador != null && Habilidad.estaActiva)
        {
            // Mover la moneda hacia el jugador
            Vector2 direccion = (jugador.transform.position - transform.position).normalized;
            float velocidadIman = controlvelocidad.GetVelocidad() * 5;
            rb.velocity = direccion * velocidadIman;
        }
        else if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }

        if (hud.ContadorHUD != null)
        {
            hud.ContadorHUD.text = ContadorMonedas.ToString();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!EstaDesactivado)
            {
                ContadorMonedas++;
                if (ContadorMonedas > RecordMonedas)
                {
                    RecordMonedas = ContadorMonedas;
                    MonedasSaver.SaveRecordMonedas(RecordMonedas); // Guardar el nuevo r�cord
                }
                movimiento.audioMoneda.Play();
                DesactivarMoneda();
            }
        }
        if (Habilidad.estaActiva)
        { }
        else
        {
            if (other.gameObject.tag == "PowerUP1")
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
            }

            if (other.gameObject.tag == "PowerUP2")
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
            }

            if (other.gameObject.tag == "Pinchos")
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
            }

            if (other.gameObject.tag == "PowerDown1")
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
            }

            if (other.gameObject.tag == "PowerDown2")
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
            }
        }
    }

    void OnBecameInvisible()
    {
        DesactivarMoneda();
    }

    public void DesactivarMoneda(Action<Monedas> desactivarAccionParametro)
    {
        desactivarAccion = desactivarAccionParametro;
    }

    private void DesactivarMoneda()
    {
        if (!EstaDesactivado)
        {
            EstaDesactivado = true;
            desactivarAccion?.Invoke(this);
            gameObject.SetActive(false);
        }
    }

    private void LoadMonedas()
    {
        (ContadorMonedas, RecordMonedas) = MonedasSaver.LoadMonedas();
        if (ContadorMonedas == 0) // Inicializar si no se encuentran datos guardados
        {
            ContadorMonedas = 0;
            RecordMonedas = 0;
        }
    }

    public void ActualizarContadorGameOver()
    {
        if (hud.ContadorGameOver != null)
        {
            hud.ContadorGameOver.text = ContadorMonedas.ToString();
        }
        if (hud.ContadorRecord != null)
        {
            hud.ContadorRecord.text = RecordMonedas.ToString();
        }
    }
}



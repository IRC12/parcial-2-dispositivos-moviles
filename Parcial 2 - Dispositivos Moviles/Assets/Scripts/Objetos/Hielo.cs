using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hielo : MonoBehaviour
{
    private Rigidbody2D rb;
    private ControlVelocidad controlvelocidad;
    public float desplazamiento = 2.0f;
    private Movimiento movimiento;
    static public float duracionHabilidad;
    public float habilidad =5;

    public void Awake()
    {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        controlvelocidad = FindObjectOfType<ControlVelocidad>();
        movimiento = FindObjectOfType<Movimiento>();
        duracionHabilidad = habilidad;
    }

    void OnEnable()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void Update()
    {
        if (controlvelocidad != null)
        {
            float velocidad = controlvelocidad.GetVelocidad();
            rb.velocity = new Vector2(0, velocidad);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Pinchos")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
        }

        if (other.gameObject.tag == "PowerUP1")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
        }

        if (other.gameObject.tag == "PowerUP1")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + desplazamiento, transform.position.z);
        }
    }
}

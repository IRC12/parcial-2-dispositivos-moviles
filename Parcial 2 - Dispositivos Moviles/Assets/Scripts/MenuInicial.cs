using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("Juego");
    }

    public void Historia()
    {
       SceneManager.LoadScene("1.TextoInicio");
    }

    public void Salir()
    {
        Application.Quit();
        Debug.Log("Salir");
    }
}

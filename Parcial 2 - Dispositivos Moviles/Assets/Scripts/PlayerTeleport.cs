using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleport : MonoBehaviour
{
    public float teleportIZQ;
    public float teleportDER;
    public float tiempoespera;
    private float elapsedtime;
    public float maxSpeed;
    private Rigidbody2D rb;
    private Vector3 savedVelocity;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        elapsedtime = 0f;
    }

    void Update()
    {
        elapsedtime += Time.deltaTime;
        if (Habilidad2.estaActiva)
        {
            if (transform.position.x > teleportDER && transform.parent == null)
            {
                Teleport(teleportIZQ);
            }

            if (transform.position.x < teleportIZQ && transform.parent == null)
            {
                Teleport(teleportDER);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Habilidad2.estaActiva)
        {
            if (other.gameObject.CompareTag("Paredderecha"))
            {
                if (transform.parent == null && elapsedtime >= tiempoespera)
                {
                    Teleport(teleportIZQ);
                }
            }

            if (other.gameObject.CompareTag("Paredizquierda"))
            {
                if (transform.parent == null && elapsedtime >= tiempoespera)
                {
                    Teleport(teleportDER);
                }
            }
        }

        
    }

    private void Teleport(float newXPosition)
    {
        elapsedtime = 0f;
        savedVelocity = rb.velocity;
        rb.isKinematic = true;

        Vector3 newPosition = transform.position;
        newPosition.x = newXPosition;
        transform.position = newPosition;

        if (savedVelocity.magnitude > maxSpeed)
        {
            savedVelocity = savedVelocity.normalized * maxSpeed;
        }

        rb.isKinematic = false;
        rb.velocity = savedVelocity;
    }
}

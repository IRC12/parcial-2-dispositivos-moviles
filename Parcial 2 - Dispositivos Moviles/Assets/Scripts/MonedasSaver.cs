using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonedasSaver : MonoBehaviour
{
    private void Start()
    {
        LoadMonedas();
    }

    private void OnApplicationQuit()
    {
        SaveMonedas();
    }

    public static void SaveMonedas()
    {
        List<PlayerData> topRecords = LoadPlayerData();

        // Si topRecords es null, inicializa una nueva lista
        if (topRecords == null)
        {
            topRecords = new List<PlayerData>();
        }

        // Actualiza el contador de monedas en los datos existentes
        bool playerDataFound = false;
        foreach (PlayerData data in topRecords)
        {
            if (data.playerName == "CurrentPlayer") // Reemplaza "CurrentPlayer" con el nombre del jugador actual
            {
                data.record = Monedas.RecordMonedas;
                playerDataFound = true;
                break;
            }
        }

        // Si no se encontr� el jugador actual en la lista, agrega un nuevo registro
        if (!playerDataFound)
        {
            topRecords.Add(new PlayerData("CurrentPlayer", Monedas.RecordMonedas)); // Reemplaza "CurrentPlayer" con el nombre del jugador actual
        }

        // Guarda los datos actualizados
        SaveManager.SavePlayerData(topRecords);
    }

    public static void SaveRecordMonedas(float record)
    {
        List<PlayerData> topRecords = LoadPlayerData();

        // Si topRecords es null, inicializa una nueva lista
        if (topRecords == null)
        {
            topRecords = new List<PlayerData>();
        }

        // Actualiza el r�cord de monedas en los datos existentes
        bool playerDataFound = false;
        foreach (PlayerData data in topRecords)
        {
            if (data.playerName == "CurrentPlayer") // Reemplaza "CurrentPlayer" con el nombre del jugador actual
            {
                if (record > data.record)
                {
                    data.record = record;
                }
                playerDataFound = true;
                break;
            }
        }

        // Si no se encontr� el jugador actual en la lista, agrega un nuevo registro
        if (!playerDataFound)
        {
            topRecords.Add(new PlayerData("CurrentPlayer", record)); // Reemplaza "CurrentPlayer" con el nombre del jugador actual
        }

        // Guarda los datos actualizados
        SaveManager.SavePlayerData(topRecords);
    }

    public static (float, float) LoadMonedas()
    {
        List<PlayerData> topRecords = LoadPlayerData();
        float contador = 0;
        float record = 0;

        // Cargar el contador de monedas de los datos guardados
        if (topRecords != null && topRecords.Count > 0)
        {
            foreach (PlayerData data in topRecords)
            {
                if (data.playerName == "CurrentPlayer") // Reemplaza "CurrentPlayer" con el nombre del jugador actual
                {
                    contador = data.record; // Este ser�a el r�cord previo, puede ajustarse seg�n lo que desees cargar
                    record = data.record;
                    break;
                }
            }
        }
        return (contador, record);
    }

    private static List<PlayerData> LoadPlayerData()
    {
        List<PlayerData> topRecords = SaveManager.LoadPlayerData();
        return topRecords;

        //return SaveManager.LoadPlayerData();
    }
}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Spawner : MonoBehaviour
{
    //Prefabs de los objetos
    [Header("Prefabs")]
    public Monedas PrefabMonedas;
    public GameObject PrefabPinchos;
    public GameObject PrefabHabilidad;
    public GameObject PrefabPowerDown;
    public GameObject PrefabPowerHielo;
    public GameObject PrefabHabilidad2;
    // Rate de aparicion de los objetos
    [Space(5)]
    [Header("Rate de Aparici�n")]
    public float RateMoneda;
    public float RatePinchos;
    public float RateHabilidad;
    public float RatePowerDown;
    public float RatePowerHielo;
    public float RateHabilidad2;
    // Tiempo de aparicion de los objetos, afectado por el rate
    private float nextMonedaSpawn;
    private float nextPinchosSpawn;
    private float nextHabilidadSpawn;
    private float nextPowerDownSpawn;
    private float nextPowerHieloSpawn;
    private float nextHabilidad2Spawn;
    // Controlar el incremento del rate de spawn
    [Space(5)]
    [Header("Aumento de Aparici�n")]
    public float incrementRateMoneda = 0.1f;
    public float incrementRatePinchos = 0.1f;
    public float incrementRateHabilidad = 0.1f;
    public float incrementRatePowerDown = 0.1f;
    public float incrementRatePowerHielo = 0.1f;
    public float incrementRateHabilidad2 = 0.1f;
    //Rate maximo que pueden alcanzar
    [Space(5)]
    [Header("Rate m�ximo de Aparici�n")]
    public float maxRateMoneda = 10f;
    public float maxRatePinchos = 10f;
    public float maxRateHabilidad = 10f;
    public float maxRatePowerDown = 10f;
    public float maxRatePowerHielo = 10f;
    public float maxRateHabilidad2 = 10f;
    //
    private Camera mainCamera;
    private ObjectPool<Monedas> monedasPool;
    private float elapsedTime = 0f;
    private Movimiento movimiento;
    private bool tutorialHecho = false;
    void Start()
    {
        mainCamera = Camera.main;
        movimiento = FindObjectOfType<Movimiento>();
        // Inicializar los tiempos de spawn para evitar la generacion de objetos al iniciar el juego
        nextMonedaSpawn = Time.time + 1f / RateMoneda;
        /*
        nextPinchosSpawn = Time.time + 1f / RatePinchos;
        nextHabilidadSpawn = Time.time + 1f / RateHabilidad;
        nextPowerDownSpawn = Time.time + 1f / RatePowerDown;
        */
        nextPinchosSpawn = float.MaxValue; // No spawnear al inicio
        nextHabilidadSpawn = float.MaxValue; // No spawnear al inicio
        nextPowerDownSpawn = float.MaxValue; // No spawnear al inicio
        nextPowerHieloSpawn = float.MaxValue; // no spawnear al inicio
        nextHabilidad2Spawn = float.MaxValue; // No spawnear al inicio

        monedasPool = new ObjectPool<Monedas>(() =>
        {
            Vector2 spawnPosition = RandomSpawnPosition();

            Monedas moneda = Instantiate(PrefabMonedas, spawnPosition, Quaternion.identity);
            moneda.DesactivarMoneda(DesactivarMonedaPool);
            return moneda;
        }, moneda =>
        {
            moneda.transform.position = RandomSpawnPosition();
            moneda.gameObject.SetActive(true);
            moneda.DesactivarMoneda(DesactivarMonedaPool);
        }, moneda =>
        {
            moneda.gameObject.SetActive(false);
        }, moneda =>
        {
            Destroy(moneda.gameObject);
        }, true, 10, 25);
    }

    void Update()
    {
        elapsedTime += Time.deltaTime;

        // Incrementar las tasas de aparici�n
        RateMoneda = Mathf.Min(RateMoneda + incrementRateMoneda * Time.deltaTime, maxRateMoneda);
        if (tutorialHecho)
        {
            RatePinchos = Mathf.Min(RatePinchos + incrementRatePinchos * Time.deltaTime, maxRatePinchos);
            RateHabilidad = Mathf.Min(RateHabilidad + incrementRateHabilidad * Time.deltaTime, maxRateHabilidad);
            RatePowerDown = Mathf.Min(RatePowerDown + incrementRatePowerDown * Time.deltaTime, maxRatePowerDown);
            RatePowerHielo = Mathf.Min(RatePowerHielo + incrementRatePowerHielo * Time.deltaTime, maxRatePowerHielo);
            RateHabilidad2 = Mathf.Min(RateHabilidad2 + incrementRateHabilidad2 * Time.deltaTime, maxRateHabilidad2);

            if (nextPinchosSpawn == float.MaxValue) nextPinchosSpawn = Time.time + 1f / RatePinchos;
            if (nextHabilidadSpawn == float.MaxValue) nextHabilidadSpawn = Time.time + 1f / RateHabilidad;
            if (nextPowerDownSpawn == float.MaxValue) nextPowerDownSpawn = Time.time + 1f / RatePowerDown;
            if (nextPowerHieloSpawn == float.MaxValue) nextPowerHieloSpawn = Time.time + 1f / RatePowerHielo;
            if (nextHabilidad2Spawn == float.MaxValue) nextHabilidad2Spawn = Time.time + 1f / RateHabilidad2;
        }

        if (Time.time > nextMonedaSpawn)
        {
            SpawnMoneda();
            nextMonedaSpawn = Time.time + 1f / RateMoneda;
        }

        if (Time.time > nextPinchosSpawn)
        {
            SpawnPinchos();
            nextPinchosSpawn = Time.time + 1f / RatePinchos;
        }

        if (Time.time > nextHabilidadSpawn && !Habilidad.estaActiva && !Habilidad2.estaActiva)
        {
            SpawnHabilidad();
            nextHabilidadSpawn = Time.time + 1f / RateHabilidad;
        }

        if (Time.time > nextHabilidad2Spawn && !Habilidad.estaActiva && !Habilidad2.estaActiva)
        {
            SpawnHabilidad2();
            nextHabilidad2Spawn = Time.time + 1f / RateHabilidad2;
        }

        if (Time.time > nextPowerDownSpawn)
        {
            SpawnPowerDown();
            nextPowerDownSpawn = Time.time + 1f / RatePowerDown;
        }

        if (Time.time > nextPowerHieloSpawn)
        {
            SpawnPowerHielo();
            nextPowerHieloSpawn = Time.time + 1f / RatePowerHielo;
        }


    }

    public void SpawnMoneda()
    {
        monedasPool.Get();
    }
    private void DesactivarMonedaPool(Monedas moneda)
    {
        monedasPool.Release(moneda);
    }

    void SpawnPinchos()
    {
        Vector2 spawnPosition = RandomSpawnPosition();
        Instantiate(PrefabPinchos, spawnPosition, Quaternion.identity);
    }

    void SpawnHabilidad()
    {
        Vector2 spawnPosition = RandomSpawnPosition();
        Instantiate(PrefabHabilidad, spawnPosition, Quaternion.identity);
    }

    void SpawnPowerDown()
    {
        Vector2 spawnPosition = RandomSpawnPosition();
        Instantiate(PrefabPowerDown, spawnPosition, Quaternion.identity);
    }

    void SpawnPowerHielo()
    {
        Vector2 spawnPosition = RandomSpawnPosition();
        Instantiate(PrefabPowerHielo, spawnPosition, Quaternion.identity);
    }

    void SpawnHabilidad2()
    {
        Vector2 spawnPosition = RandomSpawnPosition();
        Instantiate(PrefabHabilidad2, spawnPosition, Quaternion.identity);
    }

    public Vector2 RandomSpawnPosition()
    {
        float camHeight = 2f * mainCamera.orthographicSize;
        float camWidth = camHeight * mainCamera.aspect;

        float monedaHeight = PrefabMonedas.GetComponent<SpriteRenderer>().bounds.size.y;
        float monedaWidth = PrefabMonedas.GetComponent<SpriteRenderer>().bounds.size.x;

        float xPos = Random.Range(mainCamera.transform.position.x - camWidth / 2 + monedaWidth / 2,
                                  mainCamera.transform.position.x + camWidth / 2 - monedaWidth / 2);
        float yPos = mainCamera.transform.position.y - camHeight / 2 - monedaHeight / 2;

        return new Vector2(xPos, yPos);
    }

    public void PasarTutorial()
    {
        tutorialHecho = true;
    }
}


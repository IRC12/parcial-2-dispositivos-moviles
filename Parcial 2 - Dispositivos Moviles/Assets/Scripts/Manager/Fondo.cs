using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fondo : MonoBehaviour
{
    [SerializeField] private float VelocidadInicial;
    [SerializeField] public float Aceleracion;
    public float VelocidadActual;
    private Vector2 offset;
    private Material material;

    private void Awake()
    {
        material = GetComponent<SpriteRenderer>().material;
        VelocidadActual = VelocidadInicial;
    }

    void Update()
    {
        VelocidadActual -= Aceleracion * Time.deltaTime;
        offset = new Vector2(0, VelocidadActual * Time.deltaTime);
        material.mainTextureOffset += offset;
    }
}



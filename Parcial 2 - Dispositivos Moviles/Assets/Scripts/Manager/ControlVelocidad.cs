using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlVelocidad : MonoBehaviour
{
    [SerializeField] public float VelocidadInicial;
    [SerializeField] public float Aceleracion;
    public float VelocidadActual;

    private void Start()
    {
        VelocidadActual = VelocidadInicial;
    }

    private void Update()
    {
        VelocidadActual += Aceleracion * Time.deltaTime;
    }

    public float GetVelocidad()
    {
        return VelocidadActual;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using GoogleMobileAds.Sample;

public class pausaYderrota : MonoBehaviour
{
    [SerializeField] private GameObject botonPausa;
    [SerializeField] private GameObject menuPausa;
    [SerializeField] private GameObject menuGameOver;
    //HABILIDADES
    [Space(5)]
    [Header("Habilidades")]
    [SerializeField] public GameObject habilidadSinCargar;
    [SerializeField] private Image habilidadIndicator;
    private GameObject habilidadActual; // Variable para guardar el estado actual de la habilidad
    //HABILIDAD 1
    [Header("Habilidad 1")]
    [SerializeField] public GameObject habilidadCargada;
    //HABILIDAD 2
    [Header("Habilidad 2")]
    [SerializeField] public GameObject habilidadCargada2;
    //CONTADOR MONEDAS
    [Space(5)]
    [Header("Contador Monedas")]
    public TextMeshProUGUI ContadorHUD;
    public TextMeshProUGUI ContadorGameOver;
    public TextMeshProUGUI ContadorRecord;
    //GOOGLE ADS
    [Space(5)]
    [Header("Ads")]
    [SerializeField] private Button BotonAnuncio;
    private RewardedAdController rewardedAdController;
    private bool RewardUsada = false;
    //SONIDOS
    private Movimiento movimiento;

    public void Start()
    {
        movimiento = FindObjectOfType<Movimiento>();

        // Asigna la habilidad actual al inicio
        habilidadActual = habilidadSinCargar;

        if (ContadorHUD != null)
        {
            ContadorHUD.text = Monedas.ContadorMonedas.ToString();
        }

        if (habilidadIndicator != null)
        {
            habilidadIndicator.fillAmount = 0f; // Si es 0f el circulo indicador empieza vacio
            habilidadIndicator.gameObject.SetActive(true); // Inicialmente activado
        }

        rewardedAdController = FindObjectOfType<RewardedAdController>();
        BotonAnuncio.onClick.AddListener(ShowAd);

        if (rewardedAdController != null)
        {
            rewardedAdController.LoadAd();
        }
    }

    public void Pausa()
    {
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        menuPausa.SetActive(true);

        if (habilidadActual != null)
        {
            habilidadActual.SetActive(false);
        }

        if (movimiento.isPowerDownActive)
        {
            movimiento.audioPowerDown.Pause();
        }
        if (movimiento.isPowerDown2Active)
        {
            movimiento.audioPowerDownHielo.Pause();
        }
        else
        {
            movimiento.Musica.Pause();
        }
    }

    public void Reanudar()
    {
        if (habilidadActual != null)
        {
            habilidadActual.SetActive(true);
        }

        Time.timeScale = 1f;
        botonPausa.SetActive(true);
        menuPausa.SetActive(false);

        if (movimiento.isPowerDownActive)
        {
            movimiento.audioPowerDown.UnPause();
        }
        if (movimiento.isPowerDown2Active)
        {
            movimiento.audioPowerDownHielo.UnPause();
        }
        else
        {
            movimiento.Musica.UnPause();
        }
    }

    public void Reiniciar()
    {
        Habilidad.estaActiva = false; // Desactivar la habilidad al reiniciar
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        Habilidad.estaActiva = false; // Desactivar la habilidad al regresar al men�
        Time.timeScale = 1f;
        SceneManager.LoadScene("2.MenuInicial");
    }

    public void Muerte()
    {
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        menuGameOver.SetActive(true);
        if (movimiento.isPowerDownActive)
        {
            movimiento.audioPowerDown.Pause();
        }
        if (movimiento.isPowerDown2Active)
        {
            movimiento.audioPowerDownHielo.Pause();
        }
        else
        {
            movimiento.Musica.Pause();
        }

        if (!RewardUsada)
        {
            BotonAnuncio.gameObject.SetActive(true); // Mostrar el bot�n de anuncio la primera vez
        }
        else
        {
            BotonAnuncio.gameObject.SetActive(false); // Ocultar el bot�n de anuncio si ya se utilizo una vez
        }

        if (habilidadActual != null)
        {
            habilidadActual.SetActive(false);
        }

        Monedas monedas = FindObjectOfType<Monedas>();
        if (monedas != null)
        {
            monedas.ActualizarContadorGameOver();
        }
    }

    #region Ads
    private void ShowAd()
    {
        if (rewardedAdController != null && rewardedAdController.IsAdLoaded())
        {
            Debug.Log("Showing rewarded ad.");
            rewardedAdController.ShowAd();
        }
        else
        {
            if (rewardedAdController != null)
            {
                rewardedAdController.LoadAd();
            }
        }
    }

    public void ContinueGame()
    {
        Debug.Log("Continuing game.");
        menuGameOver.SetActive(false);
        habilidadActual.SetActive(true);
        RewardUsada = true;
        Time.timeScale = 1;
        if (movimiento.isPowerDownActive)
        {
            if(movimiento.isPowerDown2Active)
            {
                movimiento.audioPowerDown.UnPause();
                movimiento.audioPowerDownHielo.UnPause();

            }
            else
            {
                movimiento.audioPowerDown.UnPause();
            }
        }
        else
        {
            if (movimiento.isPowerDown2Active)
            {
                movimiento.audioPowerDownHielo.UnPause();
            }
            else
            {
                movimiento.Musica.UnPause();
            }
        }

        if (movimiento.isPowerDown2Active)
        {
            movimiento.audioPowerDownHielo.UnPause();
        }

    }
    #endregion

    #region Habilidad
    public void ActualizarHabilidad(bool cargada, int index)
    {
        if (cargada && index == 1)
        {
            if (habilidadCargada2 == true)
            {
                habilidadCargada2.SetActive(false);
                habilidadCargada.SetActive(true);
                habilidadActual = habilidadCargada;

                if (habilidadIndicator != null)
                {
                    habilidadIndicator.fillAmount = 1f; // Llena el indicador si se pone 1f
                }
            }
            else
            {
                habilidadSinCargar.SetActive(false);
                habilidadCargada.SetActive(true);
                habilidadActual = habilidadCargada;

                if (habilidadIndicator != null)
                {
                    habilidadIndicator.fillAmount = 1f; // Llena el indicador si se pone 1f
                }
            }

        }

        else if (cargada && index == 2)
        {
            if (habilidadCargada == true)
            {
                habilidadCargada.SetActive(false);
                habilidadCargada2.SetActive(true);
                habilidadActual = habilidadCargada2;

                if (habilidadIndicator != null)
                {
                    habilidadIndicator.fillAmount = 1f; // Llena el indicador si se pone 1f
                }
            }
            else
            {
                habilidadSinCargar.SetActive(false);
                habilidadCargada2.SetActive(true);
                habilidadActual = habilidadCargada2;

                if (habilidadIndicator != null)
                {
                    habilidadIndicator.fillAmount = 1f; // Llena el indicador si se pone 1f
                }
            }
        }

        else
        {
            habilidadCargada.SetActive(false);
            habilidadCargada2.SetActive(false);
            habilidadSinCargar.SetActive(true);
            habilidadActual = habilidadSinCargar;

            if (habilidadIndicator != null)
            {
                habilidadIndicator.fillAmount = 0f; // Vacia el indicador si se pone 0f
            }
        }
    }

    public void UsarHabilidad(int index)
    {
        Movimiento movimiento = FindObjectOfType<Movimiento>();
        if (movimiento != null && index == 1)
        {
            movimiento.UsarHabilidad1();
        }

        if (movimiento != null && index == 2)
        {
            movimiento.UsarHabilidad2();
        }
    }
    #endregion
}









using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour
{
    //static public bool paredes;
    [Header("Movimiento")]
    public float velocidadHorizontal = 10.0f;
    public float minX = -5.0f;
    public float maxX = 5.0f;
    private Spawner spawner;
    // POWER DOWN
    [Space(5)]
    [Header("PowerDown")]
    static public float tiempoPowerDown;
    [SerializeField] public GameObject IndicadorPowerDown;
    public float LimitDown;
    public float DelayParpadeo = 3f; // Tiempo antes de que comience el parpadeo
    public bool isPowerDownActive;
    private Coroutine ParpadeoCoroutine;
    // POWER DOWN HIELO
    [Space(5)]
    [Header("Hielo")]
    static public float tiempoPowerDown2;
    [SerializeField] public GameObject IndicadorPowerDown2;
    public bool isPowerDown2Active;
    public float LimitDown2;
    // IMAN
    [Space(5)]
    [Header("Iman")]
    [SerializeField] private float duracionHabilidad = 5f;
    [SerializeField] private Image habilidadIndicator;
    private pausaYderrota hud;
    // SONIDOS
    [Space(5)]
    [Header("Sonidos")]
    public AudioSource audioMoneda;
    public AudioSource audioPowerUP;
    public AudioSource audioPinchos;
    public AudioSource audioPowerDown;
    public AudioSource audioPowerDownHielo;
    public AudioSource Musica;


    void Start()
    {
        tiempoPowerDown = 0f;
        tiempoPowerDown2 = 0f;
        isPowerDownActive = false;
        isPowerDown2Active = false;
        hud = FindObjectOfType<pausaYderrota>();
        spawner = FindObjectOfType<Spawner>();
    }

    void Update()
    {
        if (isPowerDownActive)
        {
            // Acelerometro
            float movimientoHorizontal = Input.acceleration.x * velocidadHorizontal * Time.deltaTime * -1;
            float nuevaPosX = Mathf.Clamp(transform.position.x + movimientoHorizontal, minX, maxX);
            transform.position = new Vector3(nuevaPosX, transform.position.y, transform.position.z);

            tiempoPowerDown += Time.deltaTime; 

            if (tiempoPowerDown >= LimitDown)
            {
                StopPowerDown();
            }

            if (isPowerDown2Active)
            {
                tiempoPowerDown2 += Time.deltaTime;

                if (tiempoPowerDown2 >= LimitDown2)
                {
                    StopPowerDown2();
                }
            }
        }
       
        else if (isPowerDown2Active)
        {
            velocidadHorizontal = 0;

            tiempoPowerDown2 += Time.deltaTime;

            if (tiempoPowerDown2 >= LimitDown2)
            {
                StopPowerDown2();
            }
        }
        else
        {
            // Acelerometro
            float movimientoHorizontal = Input.acceleration.x * velocidadHorizontal * Time.deltaTime;
            float nuevaPosX = Mathf.Clamp(transform.position.x + movimientoHorizontal, minX, maxX);
            transform.position = new Vector3(nuevaPosX, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PostTutorial")
        {
            spawner.PasarTutorial();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "PowerDown1")
        {
            StartPowerDown();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "PowerDown2")
        {
            StartPowerDown2();
            other.gameObject.SetActive(false);
        }
    }

    #region PowerDown1
    private void StartPowerDown()
    {
        tiempoPowerDown = 0;
        isPowerDownActive = true;
        IndicadorPowerDown.SetActive(true);
        Musica.Pause();
        if (audioPowerDown.isPlaying)
        { }
        else
        {
            audioPowerDown.Play();
        }

        if (ParpadeoCoroutine != null)
        {
            StopCoroutine(ParpadeoCoroutine);
        }
        ParpadeoCoroutine = StartCoroutine(EfectoParpadeo());
    }

    private void StopPowerDown()
    {
        isPowerDownActive = false;
        tiempoPowerDown = 0;
        IndicadorPowerDown.SetActive(false);
        audioPowerDown.Stop();
        if (audioPowerDownHielo.isPlaying)
        { }
        else
        {
            Musica.UnPause();
        }
        if (ParpadeoCoroutine != null)
        {
            StopCoroutine(ParpadeoCoroutine);
            ParpadeoCoroutine = null;
        }
    }

    private IEnumerator EfectoParpadeo()
    {
        yield return new WaitForSeconds(DelayParpadeo);
        while (isPowerDownActive)
        {
            float remainingTime = LimitDown - tiempoPowerDown;
            float blinkSpeed = Mathf.Lerp(0.5f, 0.1f, 1 - (remainingTime / LimitDown)); // Parpadeo m�s r�pido a medida que pasa el tiempo
            IndicadorPowerDown.SetActive(!IndicadorPowerDown.activeSelf);
            yield return new WaitForSeconds(blinkSpeed);
        }
    }
    #endregion

    #region PowerDownHielo

    private void StartPowerDown2()
    {
        if (isPowerDownActive)
        {
            velocidadHorizontal = 0;
            tiempoPowerDown2 = 0;
            isPowerDown2Active = true;
            IndicadorPowerDown2.SetActive(true);
            Musica.Pause();
            if (audioPowerDownHielo.isPlaying)
            { }
            else
            {
                audioPowerDownHielo.Play();
            }
        }
        else
        {
            tiempoPowerDown2 = 0;
            isPowerDown2Active = true;
            IndicadorPowerDown2.SetActive(true);
            Musica.Pause();
            if (audioPowerDownHielo.isPlaying)
            { }
            else
            {
                audioPowerDownHielo.Play();
            }
        }
    }

    private void StopPowerDown2()
    {
        isPowerDown2Active = false;
        velocidadHorizontal = 10.0f;
        tiempoPowerDown2 = 0;
        IndicadorPowerDown2.SetActive(false);
        audioPowerDownHielo.Stop();
        if (audioPowerDown.isPlaying)
        { }
        else
        {
            Musica.UnPause();
        }
    }

    #endregion

    #region Habilidades
    public void UsarHabilidad1()
    {
        if (hud != null)
        {
            hud.ActualizarHabilidad(true, 1); // Actualizar para que se muestre como cargada
            StartCoroutine(ActivarHabilidad1());
        }
    }

    public void UsarHabilidad2()
    {
        if (hud != null)
        {
            hud.ActualizarHabilidad(true, 2); // Actualizar para que se muestre como cargada
            StartCoroutine(ActivarHabilidad2());
        }
    }

    private IEnumerator ActivarHabilidad1()
    {
        Habilidad.estaActiva = true;
        float tiempoRestante = duracionHabilidad;

        if (habilidadIndicator != null)
        {
            habilidadIndicator.fillAmount = 1f;
        }

        while (tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            if (habilidadIndicator != null)
            {
                habilidadIndicator.fillAmount = tiempoRestante / duracionHabilidad;
            }
            yield return null;
        }

        Habilidad.estaActiva = false;
        if (hud != null)
        {
            hud.ActualizarHabilidad(false, 0);
        }
    }

    private IEnumerator ActivarHabilidad2()
    {
        Habilidad2.estaActiva = true;
        float tiempoRestante = duracionHabilidad;

        if (habilidadIndicator != null)
        {
            habilidadIndicator.fillAmount = 1f;
        }

        while (tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            if (habilidadIndicator != null)
            {
                habilidadIndicator.fillAmount = tiempoRestante / duracionHabilidad;
            }
            yield return null;
        }

        Habilidad2.estaActiva = false;
        if (hud != null)
        {
            hud.ActualizarHabilidad(false, 0);
        }
    }
    #endregion
}













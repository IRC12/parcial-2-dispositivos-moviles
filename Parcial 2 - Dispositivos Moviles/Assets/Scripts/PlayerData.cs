[System.Serializable]
public class PlayerData
{
    public string playerName; // Nuevo campo para almacenar el nombre del jugador
    public float record;

    public PlayerData(string playerName, float record) // Constructor actualizado
    {
        this.playerName = playerName;
        this.record = record;
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostTutorial : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float velocidadMovimiento = 3f;

    public void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        rb.velocity = new Vector2(0, velocidadMovimiento);
    }
}

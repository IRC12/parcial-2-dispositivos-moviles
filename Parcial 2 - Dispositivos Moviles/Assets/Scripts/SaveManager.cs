using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public class SaveManager : MonoBehaviour
{
    public static void SavePlayerData(List<PlayerData> topRecords) // Recibimos la lista en lugar de un valor float
    {
        string dataPath = Application.persistentDataPath + "/topRecords.save"; // Cambiamos el nombre del archivo
        FileStream fileStream = new FileStream(dataPath, FileMode.Create);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, topRecords); // Serializamos la lista completa
        fileStream.Close();
    }

    public static List<PlayerData> LoadPlayerData() // Cambiamos el tipo de retorno a List<PlayerData>
    {
        string dataPath = Application.persistentDataPath + "/topRecords.save";

        if (File.Exists(dataPath))
        {
            FileStream fileStream = new FileStream(dataPath, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<PlayerData> topRecords = (List<PlayerData>)binaryFormatter.Deserialize(fileStream); // Deserializamos la lista completa
            fileStream.Close();
            return topRecords;
        }
        else
        {
            Debug.LogError("El archivo de guardado no existe");
            return null;
        }
    }
}

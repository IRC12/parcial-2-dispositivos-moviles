using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImanTutorial : MonoBehaviour
{
    public GameObject GIF; 
    private bool tutorialActivo = false;
    private Rigidbody2D rb;
    [SerializeField] private float velocidadMovimiento = 2f;
    public LayerMask playerLayer; 

    private Movimiento movimiento;
    private Fondo fondo;
    private Habilidad[] habilidad;
    private Monedas[] monedas;
    private Pinchos[] pinchos;
    private PowerDown[] powerDown;
    private Spawner spawner;

    public void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (((1 << other.gameObject.layer) & playerLayer) != 0 && !tutorialActivo)
        {
            tutorialActivo = true;

            movimiento = other.GetComponent<Movimiento>();
            fondo = FindObjectOfType<Fondo>();
            habilidad = FindObjectsOfType<Habilidad>();
            monedas = FindObjectsOfType<Monedas>();
            pinchos = FindObjectsOfType<Pinchos>();
            powerDown = FindObjectsOfType<PowerDown>();
            spawner = FindObjectOfType<Spawner>();

            DesactivarScripts();
            StartCoroutine(ActivarGIF(2f)); // Tiempo que va a tardar en aparecer el GIF
        }
    }

    private IEnumerator ActivarGIF(float segundos)
    {
        yield return new WaitForSeconds(segundos);
        if (tutorialActivo) 
        {
            GIF.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (tutorialActivo && ((1 << other.gameObject.layer) & playerLayer) != 0)
        {
            tutorialActivo = false;
            ActivarScripts();
        }
    }

    void OnEnable()
    {
        rb.velocity = new Vector2(0, velocidadMovimiento);
    }

    public void PinchoArrastrado()
    {
        if (tutorialActivo)
        {
            tutorialActivo = false;
            GIF.SetActive(false);
            ActivarScripts();
        }
    }

    private void DesactivarScripts()
    {
        if (movimiento != null) movimiento.enabled = false;
        if (fondo != null) fondo.enabled = false;

        foreach (var habilidad in habilidad)
        {
            if (habilidad != null) habilidad.enabled = false;
            var rbHabilidad = habilidad.GetComponent<Rigidbody2D>();
            if (rbHabilidad != null) rbHabilidad.velocity = Vector2.zero;
        }

        foreach (var moneda in monedas)
        {
            if (moneda != null) moneda.enabled = false;
            var rbMoneda = moneda.GetComponent<Rigidbody2D>();
            if (rbMoneda != null) rbMoneda.velocity = Vector2.zero;
        }

        foreach (var pincho in pinchos)
        {
            if (pincho != null) pincho.enabled = false;
            var rbPincho = pincho.GetComponent<Rigidbody2D>();
            if (rbPincho != null) rbPincho.velocity = Vector2.zero;
        }

        foreach (var powerDown in powerDown)
        {
            if (powerDown != null) powerDown.enabled = false;
            var rbPowerDown = powerDown.GetComponent<Rigidbody2D>();
            if (rbPowerDown != null) rbPowerDown.velocity = Vector2.zero;
        }

        if (spawner != null) spawner.enabled = false;

        rb.velocity = Vector2.zero; 
    }

    private void ActivarScripts()
    {
        if (movimiento != null) movimiento.enabled = true;
        if (fondo != null) fondo.enabled = true;

        foreach (var habilidad in habilidad)
        {
            if (habilidad != null) habilidad.enabled = true;
            var rbHabilidad = habilidad.GetComponent<Rigidbody2D>();
            if (rbHabilidad != null) rbHabilidad.velocity = new Vector2(0, velocidadMovimiento);
        }

        foreach (var moneda in monedas)
        {
            if (moneda != null) moneda.enabled = true;
            var rbMoneda = moneda.GetComponent<Rigidbody2D>();
            if (rbMoneda != null) rbMoneda.velocity = new Vector2(0, velocidadMovimiento);
        }

        foreach (var pincho in pinchos)
        {
            if (pincho != null) pincho.enabled = true;
            var rbPincho = pincho.GetComponent<Rigidbody2D>();
            if (rbPincho != null) rbPincho.velocity = new Vector2(0, velocidadMovimiento);
        }

        foreach (var powerDown in powerDown)
        {
            if (powerDown != null) powerDown.enabled = true;
            var rbPowerDown = powerDown.GetComponent<Rigidbody2D>();
            if (rbPowerDown != null) rbPowerDown.velocity = new Vector2(0, velocidadMovimiento);
        }

        if (spawner != null) spawner.enabled = true;

        rb.velocity = new Vector2(0, velocidadMovimiento); 
    }
}

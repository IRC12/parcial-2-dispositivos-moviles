using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class VariosTextos : MonoBehaviour
{
    public TextMeshProUGUI[] textos;
    public TextMeshProUGUI mensajeContinuar;
    public TextMeshProUGUI textoWaffly;
    public float velocidadEscritura;
    public string siguienteEscena;
    [SerializeField] private GameObject pantallaWaffly;
    private int indiceTextoActual = 0;
    private bool puedeContinuar = false;
    private bool pantallaWafflyActiva = false;
    private bool intermediaMostrada = false; // Bandera para mostrar pantalla Waffly despu�s del primer texto
    private bool textoEscribiendose = false;

    void Start()
    {
        mensajeContinuar.gameObject.SetActive(false);
        pantallaWaffly.SetActive(false);
        foreach (var texto in textos)
        {
            texto.gameObject.SetActive(false);
        }
        MostrarSiguienteTexto();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (pantallaWafflyActiva)
            {
                if (textoEscribiendose)
                {
                    textoEscribiendose = false;
                }
                else
                {
                    pantallaWaffly.SetActive(false);
                    pantallaWafflyActiva = false;
                    MostrarSiguienteTexto();
                }
            }
            else if (textoEscribiendose)
            {
                textoEscribiendose = false;
            }
            else if (puedeContinuar)
            {
                AvanzarTexto();
            }
        }
    }

    void MostrarSiguienteTexto()
    {
        if (indiceTextoActual < textos.Length)
        {
            textos[indiceTextoActual].gameObject.SetActive(true); // Activar el siguiente texto
            StartCoroutine(EscribirTexto(textos[indiceTextoActual])); // Escribir el texto
        }
        else
        {
            SceneManager.LoadScene(siguienteEscena);
        }
    }

    IEnumerator EscribirTexto(TextMeshProUGUI textoAMostrar)
    {
        mensajeContinuar.gameObject.SetActive(false); // Ocultar el mensaje de continuar al empezar a escribir
        string textoOriginal = textoAMostrar.text;
        textoAMostrar.text = ""; // Limpiar el texto antes de empezar a escribir
        textoEscribiendose = true;

        foreach (char caracter in textoOriginal)
        {
            if (!textoEscribiendose)
            {
                textoAMostrar.text = textoOriginal; // Completar el texto al tocar la pantalla
                break;
            }
            textoAMostrar.text += caracter;
            yield return new WaitForSeconds(velocidadEscritura);
        }

        textoEscribiendose = false;
        puedeContinuar = true;
        mensajeContinuar.gameObject.SetActive(true);
    }

    void AvanzarTexto()
    {
        puedeContinuar = false;
        mensajeContinuar.gameObject.SetActive(false);
        textos[indiceTextoActual].gameObject.SetActive(false); // Desactivar el texto actual
        indiceTextoActual++;

        if (indiceTextoActual == 1 && !intermediaMostrada) // Mostrar pantalla intermedia despu�s del primer texto
        {
            MostrarPantallaIntermedia();
            intermediaMostrada = true;
        }
        else if (indiceTextoActual < textos.Length)
        {
            MostrarSiguienteTexto();
        }
        else
        {
            SceneManager.LoadScene(siguienteEscena); // Cambiar de escena cuando todos los textos se mostraron
        }
    }

    void MostrarPantallaIntermedia()
    {
        pantallaWaffly.SetActive(true);
        pantallaWafflyActiva = true;
        StartCoroutine(EscribirTextoWaffly(textoWaffly)); // Escribir el texto de la pantalla intermedia
    }

    IEnumerator EscribirTextoWaffly(TextMeshProUGUI textoAMostrar)
    {
        mensajeContinuar.gameObject.SetActive(false); // Ocultar el mensaje de continuar al empezar a escribir
        string textoOriginal = textoAMostrar.text;
        textoAMostrar.text = ""; // Limpiar el texto antes de empezar a escribir
        textoEscribiendose = true;

        foreach (char caracter in textoOriginal)
        {
            if (!textoEscribiendose)
            {
                textoAMostrar.text = textoOriginal; // Completar el texto al tocar la pantalla
                break;
            }
            textoAMostrar.text += caracter;
            yield return new WaitForSeconds(velocidadEscritura);
        }
        textoEscribiendose = false;
        mensajeContinuar.gameObject.SetActive(true);
    }
}



























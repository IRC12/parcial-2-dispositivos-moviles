using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrastrar : MonoBehaviour
{
    private bool arrastrando = false;
    private Vector2 ScreenPosition;
    private Vector3 WorldPosition;
    private Arrastable ultimoar;

    void Update()
    {
        if (arrastrando && (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended ))
        {
            soltar();
            return;

        }
        if (Input.touchCount > 0)
        {
            ScreenPosition = Input.GetTouch(0).position;
        }
        else
        {
            return;
        }

        WorldPosition = Camera.main.ScreenToWorldPoint(ScreenPosition);

        if(arrastrando)
        {
            arrastrar();
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(WorldPosition, Vector2.zero);
            if (hit.collider !=null)
            {
                Arrastable arrastrado = hit.transform.gameObject.GetComponent<Arrastable>();
                if(arrastrado != null)
                {
                    ultimoar = arrastrado;
                    inicial();
                }
            }
        }

    }

    void inicial()
    {
        arrastrando = true;
    }

    void arrastrar()
    {
        ultimoar.transform.position = new Vector2(WorldPosition.x, WorldPosition.y);
    }

    void soltar()
    {
        arrastrando = false;

        // Notificar al TutorialTrigger que se ha arrastrado un pincho
        ImanTutorial tutorial = FindObjectOfType<ImanTutorial>();
        if (tutorial != null)
        {
            tutorial.PinchoArrastrado();
        }
    }
}
